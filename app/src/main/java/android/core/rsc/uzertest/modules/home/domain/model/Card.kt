package android.core.rsc.uzertest.modules.home.domain.model

import android.graphics.drawable.Drawable

data class Card(val title: String, val drawable: Drawable)
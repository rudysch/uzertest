package android.core.rsc.uzertest.modules.detailitem.presentation

import android.content.Context
import android.core.rsc.uzertest.R
import android.core.rsc.uzertest.modules.home.domain.mapper.toCardViewData
import android.core.rsc.uzertest.modules.home.domain.model.Card
import android.core.rsc.uzertest.modules.home.presentation.adapter.cards.CardItem
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class DetailItemFragmentViewModel(val context: Context) : ViewModel(), LifecycleObserver {
    val cardItems = MutableLiveData<List<CardItem>>()

    val drawables = listOf<Int>(R.drawable.item_1,
        R.drawable.item_2,
        R.drawable.item_3,
        R.drawable.item_4,
        R.drawable.item_5,
        R.drawable.item_6,
        R.drawable.item_7,
        R.drawable.item_8,
        R.drawable.item_9,
        R.drawable.item_10,
        R.drawable.item_11,
        R.drawable.item_12,
        R.drawable.item_13)

    init {
        val cards: ArrayList<Card> = ArrayList()

        for (i in 1..5) {
            getRandomDrawable()?.let { Card("Item $i", it) }?.let { cards.add(it) }
        }

        cardItems.value = cards.map { card -> card.toCardViewData() }
    }

    private fun getRandomDrawable(): Drawable? {
        val drawable = drawables.random()
        return ContextCompat.getDrawable(context, drawable)
    }
}
package android.core.rsc.uzertest.modules.detailitem

import android.core.rsc.uzertest.common.interfaces.IKoinModule
import android.core.rsc.uzertest.modules.detailitem.presentation.DetailItemFragment
import android.core.rsc.uzertest.modules.detailitem.presentation.DetailItemFragmentViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.fragment.dsl.fragment
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object KoinDetailItemModuleUzerTest: IKoinModule {
    private val homeModule = module {
        fragment { DetailItemFragment() }
        viewModel { DetailItemFragmentViewModel(androidContext()) }
    }

    override val modules: List<Module> = listOf(homeModule)
}
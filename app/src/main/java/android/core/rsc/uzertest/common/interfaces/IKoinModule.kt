package android.core.rsc.uzertest.common.interfaces

interface IKoinModule {
    val modules: List<org.koin.core.module.Module>
}
package android.core.rsc.uzertest.ui.generic

import androidx.lifecycle.MutableLiveData

interface TextViewData {
    val text: MutableLiveData<CharSequence?>
    val hint: MutableLiveData<CharSequence?>
    val textAppearance: MutableLiveData<Int>
    val textAccessibility: MutableLiveData<String>

    open class Implementation : TextViewData {
        companion object {
            const val EMPTY = ""
        }

        override val text: MutableLiveData<CharSequence?> = MutableLiveData<CharSequence?>(null)
        override val hint: MutableLiveData<CharSequence?> = MutableLiveData<CharSequence?>(null)
        override val textAccessibility: MutableLiveData<String> = MutableLiveData(EMPTY)
        override val textAppearance: MutableLiveData<Int> = MutableLiveData(0)

    }
}
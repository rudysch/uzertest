package android.core.rsc.uzertest.modules.home.presentation.adapter.cards

import android.annotation.SuppressLint
import android.core.rsc.uzertest.modules.home.presentation.adapter.cards.holder.CardItemViewHolder
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView

/** Adapter for listing cards in home page */
class CardRecyclerViewAdapter(
    val lifecycle: LifecycleOwner
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val items = mutableListOf<CardItem>()

    @SuppressLint("NotifyDataSetChanged")
    fun updateData(cardItems: List<CardItem>) {
        items.clear()
        items.addAll(cardItems)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = items.size
    override fun getItemId(position: Int): Long = items[position].hashCode().toLong()
    override fun getItemViewType(position: Int): Int = items[position].type.ordinal

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CardItemViewHolder(CardItemViewHolder.inflate(parent))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (val item = items[position]) {
            is CardItem.Card -> (holder as? CardItemViewHolder)?.bind(lifecycle, item)
        }
    }
}
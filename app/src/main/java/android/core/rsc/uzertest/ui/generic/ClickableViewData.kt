package android.core.rsc.uzertest.ui.generic

import android.view.View
import androidx.lifecycle.MutableLiveData

interface ClickableViewData {
    val clickable: MutableLiveData<Boolean>
    val enabled: MutableLiveData<Boolean>
    var onClick: ((View) -> Unit)?
    fun performClick(view: View) = onClick?.invoke(view)

    open class Implementation : ClickableViewData {
        override val clickable: MutableLiveData<Boolean> = MutableLiveData(true)
        override val enabled: MutableLiveData<Boolean> = MutableLiveData(false)
        override var onClick: ((View) -> Unit)? = null
            set(value) {
                field = value
                clickable.postValue(value != null)
            }
    }
}
package android.core.rsc.uzertest.modules.home.presentation

import android.core.rsc.uzertest.R
import android.core.rsc.uzertest.common.BaseFragment
import android.core.rsc.uzertest.databinding.FragmentHomeBinding
import android.core.rsc.uzertest.modules.home.presentation.adapter.cards.CardRecyclerViewAdapter
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import org.koin.android.viewmodel.ext.android.viewModel

class HomeFragment : BaseFragment<FragmentHomeBinding>() {
    private val viewModel: HomeFragmentViewModel by viewModel()

    override val layoutId: Int
        get() = R.layout.fragment_home

    override fun bindViewModels(binding: FragmentHomeBinding) {
        binding.viewModel = viewModel
        binding.lifecycle = lifecycle
        lifecycle.lifecycle.addObserver(viewModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        setLiveDataObservers()
    }

    private fun setLiveDataObservers() {
        viewModel.cardItems.observe(viewLifecycleOwner) {
            ((binding.cardRecyclerView.adapter) as CardRecyclerViewAdapter).updateData(it)
            ((binding.cardRecyclerView2.adapter) as CardRecyclerViewAdapter).updateData(it)
            ((binding.cardRecyclerView3.adapter) as CardRecyclerViewAdapter).updateData(it)
            ((binding.cardRecyclerView4.adapter) as CardRecyclerViewAdapter).updateData(it)
            ((binding.cardRecyclerView5.adapter) as CardRecyclerViewAdapter).updateData(it)
            ((binding.cardRecyclerView6.adapter) as CardRecyclerViewAdapter).updateData(it)
        }
    }

    private fun initRecyclerView() {
        val recyclerviews = listOf(binding.cardRecyclerView,
            binding.cardRecyclerView2,
            binding.cardRecyclerView3,
            binding.cardRecyclerView4,
            binding.cardRecyclerView5,
            binding.cardRecyclerView6
        )

        recyclerviews.forEach { recyclerView ->
            recyclerView.apply {
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
                adapter = CardRecyclerViewAdapter(
                    viewLifecycleOwner
                )
            }
        }
    }
}

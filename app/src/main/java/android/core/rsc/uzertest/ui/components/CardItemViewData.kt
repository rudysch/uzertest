package android.core.rsc.uzertest.ui.components

import android.core.rsc.uzertest.ui.generic.ClickableViewData
import android.core.rsc.uzertest.ui.generic.TextViewData
import android.core.rsc.uzertest.ui.generic.ImageViewData

/**Item for recyclerView at home page*/
class CardItemViewData: TextViewData by TextViewData.Implementation(), ImageViewData by ImageViewData.Implementation(), ClickableViewData by ClickableViewData.Implementation()
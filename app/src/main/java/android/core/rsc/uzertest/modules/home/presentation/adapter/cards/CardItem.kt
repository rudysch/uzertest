package android.core.rsc.uzertest.modules.home.presentation.adapter.cards

import android.core.rsc.uzertest.ui.components.CardItemViewData

/**
 *  Item for [HomeListRecyclerViewAdapter]
 */
sealed class CardItem(val type: Type) {
    enum class Type { CARD }

    data class Card(val viewData: CardItemViewData) : CardItem(Type.CARD)
}
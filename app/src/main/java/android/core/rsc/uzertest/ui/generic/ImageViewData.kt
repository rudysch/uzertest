package android.core.rsc.uzertest.ui.generic

import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData

interface ImageViewData {
    companion object {

        @JvmStatic
        @BindingAdapter("image_view_data_icon_with_visibility")
        fun setDrawableOrHideOnImageView(imageView: ImageView, icon: Drawable?) {
            imageView.isVisible = icon != null
            imageView.setImageDrawable(icon)
        }

        @JvmStatic
        @BindingAdapter("image_view_data_icon")
        fun setDrawableOnImageView(imageView: ImageView, icon: Drawable?) {
            icon?.let { imageView.setImageDrawable(it) }
        }

        @JvmStatic
        @BindingAdapter("image_view_data_visibility")
        fun setVisibility(imageView: ImageView, visible: Boolean) {
            imageView.isVisible = visible
        }

        @JvmStatic
        @BindingAdapter("image_view_data_tint")
        fun setTintColorOnImageView(imageView: ImageView, color: Int) {
            if (color != 0) imageView.imageTintList = ColorStateList.valueOf(color)
        }
    }

    val drawable: MutableLiveData<Drawable?>
    val drawableTint: MutableLiveData<Int>
    val drawableDescription: MutableLiveData<String>

    open class Implementation: ImageViewData {
        override val drawable: MutableLiveData<Drawable?> = MutableLiveData()
        override val drawableTint: MutableLiveData<Int> = MutableLiveData()
        override val drawableDescription: MutableLiveData<String> = MutableLiveData()
    }
}
package android.core.rsc.uzertest.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

abstract class BaseFragment<VB : ViewDataBinding> : Fragment() {

    /** The ViewDataBinding used by the view. */
    lateinit var binding: VB

    /** The resource layout Id used by extended fragment. */
    abstract val layoutId: Int

    /** When the binding happen this function is called to bind variables. */
    abstract fun bindViewModels(binding: VB)

    /**
     * In case the extended class use a different lifecycle for its needs.
     * @see getViewLifecycleOwner A gentle reminder to use the correct lifecycle for the needs.
     * */
    open val lifecycle get() = viewLifecycleOwner

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        binding.lifecycleOwner = lifecycle
        binding.setVariable(BR.lifecycle, lifecycle)
        bindViewModels(binding)
        return binding.root
    }
}
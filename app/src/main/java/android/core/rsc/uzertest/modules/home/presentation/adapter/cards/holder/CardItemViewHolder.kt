package android.core.rsc.uzertest.modules.home.presentation.adapter.cards.holder

import android.core.rsc.uzertest.databinding.UiCardItemBinding
import android.core.rsc.uzertest.modules.home.presentation.adapter.cards.CardItem
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView

/**
 * Component as part of [HomeRecyclerViewAdapter] viewHolders
 */
class CardItemViewHolder(private val binding: UiCardItemBinding) : RecyclerView.ViewHolder(binding.root) {
    companion object {
        fun inflate(parent: ViewGroup) = UiCardItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    }

    fun bind(lifecycle: LifecycleOwner, item: CardItem.Card) {
        binding.apply {
            viewData = item.viewData
            lifecycleOwner = lifecycle
        }
    }
}
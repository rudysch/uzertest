package android.core.rsc.uzertest.modules.home

import android.core.rsc.uzertest.modules.home.presentation.HomeFragment
import android.core.rsc.uzertest.modules.home.presentation.HomeFragmentViewModel
import android.core.rsc.uzertest.common.interfaces.IKoinModule
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.fragment.dsl.fragment
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object KoinHomeModuleUzerTest: IKoinModule {
    private val homeModule = module {
        fragment { HomeFragment() }
        viewModel { HomeFragmentViewModel(androidContext()) }
    }

    override val modules: List<Module> = listOf(homeModule)
}
package android.core.rsc.uzertest.modules.detailitem.presentation

import android.core.rsc.uzertest.R
import android.core.rsc.uzertest.common.BaseFragment
import android.core.rsc.uzertest.databinding.FragmentDetailItemBinding
import org.koin.android.viewmodel.ext.android.viewModel

class DetailItemFragment : BaseFragment<FragmentDetailItemBinding>() {
    private val viewModel: DetailItemFragmentViewModel by viewModel()

    override val layoutId: Int
        get() = R.layout.fragment_home

    override fun bindViewModels(binding: FragmentDetailItemBinding) {
        binding.viewModel = viewModel
        binding.lifecycle = lifecycle
        lifecycle.lifecycle.addObserver(viewModel)
    }
}

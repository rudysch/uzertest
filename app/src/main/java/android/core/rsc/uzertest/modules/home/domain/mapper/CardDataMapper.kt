package android.core.rsc.uzertest.modules.home.domain.mapper

import android.core.rsc.uzertest.modules.home.domain.model.Card
import android.core.rsc.uzertest.modules.home.presentation.adapter.cards.CardItem
import android.core.rsc.uzertest.ui.components.CardItemViewData

fun Card.toCardViewData() = CardItem.Card(viewData = CardItemViewData().apply {
    text.value = this@toCardViewData.title
    drawable.value = this@toCardViewData.drawable
})
package android.core.rsc.uzertest

import android.app.Application
import android.core.rsc.uzertest.modules.detailitem.KoinDetailItemModuleUzerTest
import android.core.rsc.uzertest.modules.home.KoinHomeModuleUzerTest
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.fragment.koin.fragmentFactory
import org.koin.core.KoinExperimentalAPI
import org.koin.core.context.startKoin

@KoinExperimentalAPI
class UzerTestApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@UzerTestApplication)
            fragmentFactory()
            modules(KoinHomeModuleUzerTest.modules)
            modules(KoinDetailItemModuleUzerTest.modules)
        }
    }
}